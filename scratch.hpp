#ifndef SCRATCH_HPP
#define SCRATCH_HPP
#include <cheerp/clientlib.h>


namespace client {
// None of these are complete and fully correct,
// but they are compatible

class Runtime : public Object {
public:
    void on(String *, EventListener*);
};

class Sprite : public Object {
public:
    Runtime *get_runtime() const;
    Object *get_blocks() const;
    String *get_name() const;
    Array *get_sounds() const;
    Array *get_clones() const;
    Object *get_soundBank() const;
};

class Target : public Object {
public:
    Runtime *get_runtime() const;
    String *get_id() const;
    Map *get_variables() const;
    Map *get_comments() const;
};
class RenderedTarget : public Target {
public:
    Object *get_sprite() const;
    Object *get_renderer() const;
    int get_drawableID() const;
    bool get_dragging() const;
    Map *get_effects() const;
    bool get_isOriginal() const;
    int get_x() const;
    int get_y() const;
    int get_direction() const;
    bool get_draggable() const;
    bool get_visible() const;
    int get_size() const;
    int get_currentCostume() const;
    String *get_rotationStyle() const;
    int get_volume() const;
    int get_tempo() const;
    int get_videoTransparency() const;
    String *get_videoState() const;
    String *get_textToSpeechLanguage() const;
};
}
#endif // SCRATCH_HPP
