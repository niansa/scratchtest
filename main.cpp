#include "scratch.hpp"

#include <string>
#include <format>
#include <cheerp/clientlib.h>



// An ugly boilerplate function for arbitrary operator[] access
template<typename T> __attribute((always_inline))
T getMemberByName(client::Object *object, const char *name) {
    T fres;
    __asm__("%1[%2]" : "=r"(fres) : "r"(object), "r"(new client::String(name)));
    return fres;
}

// Another ugly string concatenation template to reduce code size
client::String *concatStrings(const std::initializer_list<const client::String *>& strings) {
    auto fres = new client::String;
    for (const auto string : strings) {
        fres = fres->concat(string);
    }
    return fres;
}

// An actually nice C++ string operator to get a JS string from a litteral
__attribute((always_inline))
auto *operator"" _JS(const char *str, unsigned int) {
    return new client::String(str);
}


// The extension class
class [[cheerp::jsexport]] ScratchFrame {
    client::Runtime *runtime;
    client::HTMLElement *iFrame;
    bool enabled = false;

    struct Transform {
        struct Position {
            int x = 100, y = 100;
        } position;
        struct Size {
            int width = 350, height = 200;
        } size;
    } transform;

public:
    ScratchFrame(client::Runtime *runtime) :
      runtime(runtime) {
        iFrame = client::document.createElement("iframe");
    }

    client::Object *getInfo() const {
        client::Object *fres;
        __asm__(R"({
                "id": "Frame",
                "name": "Frame",
                "blocks": [
                    {
                        "opcode": "iFrameGet",
                        "blockType": "reporter",
                        "text": "URL of iFrame",
                        "arguments": {}
                    },
                    {
                        "opcode": "iFrameSet",
                        "blockType": "command",
                        "text": "Enable iFrame at [url]",
                        "arguments": {
                            "url": {
                                "type": "string",
                                "defaultValue": "https://example.com/"
                            }
                        }
                    },
                    {
                        "opcode": "iFrameSync",
                        "blockType": "command",
                        "text": "Transform iFrame to character",
                        "arguments": {}
                    },
                    {
                        "opcode": "iFrameTransform",
                        "blockType": "command",
                        "text": "Transform iFrame: [x], [y] / [width], [height]",
                        "arguments": {
                            "x": {
                                "type": "number",
                                "defaultValue": 10
                            },
                            "y": {
                                "type": "number",
                                "defaultValue": 10
                            },
                            "width": {
                                "type": "number",
                                "defaultValue": 350
                            },
                            "height": {
                                "type": "number",
                                "defaultValue": 200
                            }
                        }
                    },
                    {
                        "opcode": "iFrameReset",
                        "blockType": "command",
                        "text": "Disable iFrame",
                        "arguments": {}
                    }
                ],
            })" : "=r"(fres) :);
        return fres;
    }

    auto iFrameGet(client::Object *) const {
        return iFrame->getAttribute("src");
    }
    void iFrameSet(client::Object *args) {
        if (!enabled) {
            client::document.get_body()->appendChild(iFrame);
            enabled = true;
        }
        iFrame->setAttribute("src", getMemberByName<client::String*>(args, "url"));
        iFrameUpdate();
    }
    void iFrameReset(client::Object *) {
        if (!enabled) return;
        enabled = false;
        iFrame->setAttribute("src", "");
        client::document.get_body()->removeChild(iFrame);
    }

    void iFrameTransform(client::Object *args) {
        transform.position.x = getMemberByName<int>(args, "x");
        transform.position.y = getMemberByName<int>(args, "y");
        transform.size.width = getMemberByName<int>(args, "width");
        transform.size.height = getMemberByName<int>(args, "height");
        iFrameUpdate();
    }

    void iFrameSync(client::Object *, client::Object *util) {
        auto target = getMemberByName<client::RenderedTarget*>(util, "target");
        transform.position.x = target->get_x();
        transform.position.y = target->get_y();
        //transform.size.width = getMemberByName<int>(args, "width");  TODO
        //transform.size.height = getMemberByName<int>(args, "height");  TODO
        iFrameUpdate();
    }

    void iFrameUpdate() {
        using client::String;
        auto style = concatStrings({
                                       "position: absolute; "
                                       "z-index: 1000; "
                                       "top: "_JS, new String(transform.position.y), "px; "
                                       "left: "_JS, new String(transform.position.x), "px; "
                                       "width: "_JS, new String(transform.size.width), "px; "
                                       "height: "_JS, new String(transform.size.height), "px;"_JS
                                   });
        iFrame->setAttribute("style", style);
    }
} *extensionInstance;


// Some javascript crap to load the extension
void webMain() {
    __asm__("var extensionInstance = new ScratchFrame(window.vm.extensionManager.runtime)");
    __asm__("var serviceName = window.vm.extensionManager._registerInternalExtension(extensionInstance)");
    __asm__("window.vm.extensionManager._loadedExtensions.set(extensionInstance.getInfo().id, serviceName)");
}
